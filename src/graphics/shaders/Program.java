/**
 * Represents a shader object
 */

package graphics.shaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

public class Program {

	private int programId;

	public Program(int vertexShaderId, int fragmentShaderId, Context context) {
        String vertexShader   = readShaderFromFile(vertexShaderId, context);
        String fragmentShader = readShaderFromFile(fragmentShaderId, context);

        int loadedVertexShader   = loadShader( GLES20.GL_VERTEX_SHADER, vertexShader);
        int loadedFragmentShader = loadShader( GLES20.GL_FRAGMENT_SHADER, fragmentShader);

        programId = createProgram(loadedVertexShader, loadedFragmentShader);
    }

	/**************************
	 * OTHER METHODS
	 *************************/

	private int createProgram(int vertexShader, int fragmentShader) {

		int program = GLES20.glCreateProgram();
		if (program != 0) {
			GLES20.glAttachShader(program, vertexShader);
			GLES20.glAttachShader(program, fragmentShader);
			GLES20.glLinkProgram(program);
			int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
			if (linkStatus[0] != GLES20.GL_TRUE) {
				Log.e("Program", "Could not link programId: ");
				Log.e("Program", GLES20.glGetProgramInfoLog(program));
				GLES20.glDeleteProgram(program);
				return 0;
			}
		} else {
			Log.d("CreateProgram", "Could not create programId");
        }

		return program;
	}

	/**
	 * Loads a shader (either vertex or pixel) given the source
	 * @param shaderType VERTEX or PIXEL
	 * @param source The string data representing the shader code
	 * @return handle for shader
	 */
	private int loadShader(int shaderType, String source) {
		int shader = GLES20.glCreateShader(shaderType);
		if (shader != 0) {
			GLES20.glShaderSource(shader, source);
			GLES20.glCompileShader(shader);
			int[] compiled = new int[1];
			GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
			if (compiled[0] == 0) {
				Log.e("Program", "Could not compile shader " + shaderType + ":");
				Log.e("Program", GLES20.glGetShaderInfoLog(shader));
				GLES20.glDeleteShader(shader);
				shader = 0;
			}
		}
		return shader;
	}

    private String readShaderFromFile(int rawFileId, Context context) {
        StringBuffer   vs          = new StringBuffer();
        InputStream    inputStream = context.getResources().openRawResource(rawFileId);
        BufferedReader in          = new BufferedReader(new InputStreamReader(inputStream));

        if (inputStream != null) {
            String read = null;
            do {
                try {
                    read = in.readLine();
                } catch (IOException e) {
                    Log.e("ERROR-readingShader", "Could not read shader: " + e.toString());
                }
                if (read != null) {
                    vs.append(read);
                    vs.append("\n");
                }

            } while (read != null);
        }
        return vs.toString();
    }

	public int getProgramId() {
		return programId;
	}
}
