package graphics.shaders.vertex;

import graphics.shaders.util.adt.array.ArrayUtils;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

public class FloatVertices extends Vertices<FloatVertex> {

    @Override
    public Buffer getBuffer() {
        FloatBuffer buffer = (FloatBuffer) super.getBuffer();

        List<Float> list = new ArrayList<Float>();
        for (int i = 0; i < vertices.size(); i++) {
            list.addAll(vertices.get(i).getVertex());
        }
        float[] array = ArrayUtils.toFloatArray(list);

        buffer.put(array);
        return buffer;
    }

}