package graphics.shaders.vertex;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.StringTokenizer;


public class ShortVertex extends Vertex<Short> {

    public ShortVertex(Short x, Short y, Short z) {
        super(x, y, z);
    }

    @Override
    public Short getZero() {
        return 0;
    }

    @Override
    public Buffer allocateBuffer(int size) {
        int floatSizeInBytes = Float.SIZE / 8;
        int bufferSize = floatSizeInBytes * size;
        return ByteBuffer.allocateDirect(bufferSize).order(ByteOrder.nativeOrder()).asShortBuffer();
    }

}
